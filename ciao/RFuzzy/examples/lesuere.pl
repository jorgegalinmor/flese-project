:- module(lesuere,_,[rfuzzy, clpr]).

define_database(restaurant/7, 
	[(rest_name, rfuzzy_string_type), 
	  (restaurant_type, rfuzzy_enum_type), 
	   (food_type, rfuzzy_enum_type),
	    (years_since_opening, rfuzzy_integer_type), 
	     (distance_to_the_city_center, rfuzzy_integer_type), 
	      (price_average, rfuzzy_integer_type), 
	       (menu_price, rfuzzy_integer_type)]).


restaurant(kenzo,                           fast_casual,     japanese,           5,    null,       50,     null).
restaurant(burguer_king,               fast_food,        american,         10,    null,       10,     5).
restaurant(pizza_jardin,                 fast_casual,     italian,                5,    null,       15,     null).
restaurant(subway,                         fast_food,       sandwiches,        5,    null,      15,      10).
restaurant(derroscas,                     fast_food,        mediterranean,   3,    null,      25,      null).
restaurant(il_tempietto,                  fast_casual,    italian,                5,    null,       20,     null).
restaurant(kono_pizza,                   fast_food,       pizza,                  4,    null,      15,      null).
restaurant(paellador,                       fast_food,       paella,                8,     null,      40,     null).
restaurant(tapasbar,                        fast_food,       tapas,                 3,     null,      10,     null).
restaurant(meson_del_jamon,        fast_food,       spanish,              8,     100,      20,     15).
restaurant(museo_del_jamon,        fast_food,       spanish,              8,     150,      20,     15).
restaurant(zalacain,                         fine_dining,    basque,             15,     null,      60,     50).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

define_similarity_between(restaurant, food_type(spanish), food_type(tapas), 0.7).
define_similarity_between(restaurant, food_type(mediterranean), food_type(spanish), 0.8) cred (prod, 0.9).
define_similarity_between(restaurant, food_type(mediterranean), food_type(italian), 0.8) cred (prod, 0.9).
define_similarity_between(restaurant, food_type(spanish), food_type(mediterranean), 0.6) cred (prod, 0.9).
define_similarity_between(restaurant, food_type(italian), food_type(mediterranean), 0.6) cred (prod, 0.9).

near_the_city_center(restaurant) :~ defaults_to(0).
near_the_city_center(restaurant):~ function(distance_to_the_city_center(restaurant), [ (0, 1), (100, 1), (1000, 0.1) ]).

traditional(restaurant) :~ function(years_since_opening(restaurant), [ (0, 1), (5, 0.2), (10, 0.8), (15, 1), (100, 1) ]).
traditional(restaurant) :~ function(years_since_opening(restaurant), [ (0, 1), (5, 0.2), (10, 0.8), (15, 1), (100, 1) ]) only_for_user bartolo.

cheap(restaurant) :~ defaults_to(0.5).
cheap(restaurant) :~ defaults_to(0.2) if (near_the_city_center(restaurant) is_over 0.7).
cheap(restaurant) :~ value(0.1) if (rest_name(restaurant) equals zalacain).
cheap(restaurant) :~ function(price_average(restaurant), [ (0, 1), (10, 1), (15, 0.9), (20, 0.8), (30, 0.5), (50, 0.1), (100, 0) ]).

unexpensive(restaurant) :~ synonym_of(cheap(restaurant), prod, 1).
expensive(restaurant) :~ antonym_of(cheap(restaurant), prod, 1).

define_modifier(rather/2, TV_In, TV_Out) :-
	TV_Out .=. TV_In * TV_In.
define_modifier(very/2, TV_In, TV_Out) :-
	TV_Out .=. TV_In * TV_In * TV_In.
define_modifier(little/2, TV_In, TV_Out) :-
	TV_Out * TV_Out .=. TV_In.
define_modifier(very_little/2, TV_In, TV_Out) :-
	TV_Out * TV_Out * TV_Out .=. TV_In.

tempting_restaurant(restaurant) :~ defaults_to( 0.1).
tempting_restaurant(restaurant) :~ rule(min, (near_the_city_center(restaurant), fnot(very(expensive(restaurant))), very(traditional(restaurant)))) with_credibility (min, 0.7).
tempting_restaurant(restaurant) :~ rule(near_the_city_center(restaurant)) with_credibility (min, 0.5) .


