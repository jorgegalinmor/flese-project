-Added option to add new fuzzification in the select in fuzzifications/list.jsp.
-Added function void newFuzz() in FuzzificationsManager which is called when you select the new fuzzification option, retrieves the list of fields in the database with their type and forwards it to fuzzifications/new.jsp.
-Modified personalizationFunctionChanged in fuzzificationsJS.jsp so it calls newFuzz instead of edit when you select new fuzzification option.
-Added function ProgramPartAnalysis[][] getProgramFields() in ProgramAnalysis that returns an array of arrays (so it can be stored in the resultsStoreHouse's field programPartAnalysis) containing one ProgramPartAnalysis which contains a matrix nx2 containing all n database fields and their types in the second column.
-Modified ProgramAnalysis constructor so when it detects that a ProgramPartAnalysis contains a database definition, it stores it's fields.
-Added function boolean isFieldsDef() in ProgramPartAnalysis that returns true if the ProgramPartAnalysis contains a database definition.
-Modified function void parseHead() in ProgramPartAnalysis so if the ProgramPartAnalysis is a database definition it stores its fields and types in a nx2 matrix.
-Added function String[][] getProgramFields() in ProgramPartAnalysis that returns the matrix nx2 containing the program fields and its types.



-----TO DO-----

-fuzzifications/new.jsp has to be designed
    ·It should contain a field to specify the name of the fuzzification, a select to select in which field its applied, and depending of the type of the field some fields to determinate thevalue of at least 2 points.
-fuzzifications/new.jsp should call a function that writes the new function in the prgram file.
-The translaters from other formats to csv have to be added.
